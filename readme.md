## Calculator using words
In this project i am making a calculator that takes in a string with mathoperations using word ex(two times three). It contains all operators and will be able to take inputs up to 999. After convering the words and calculating it will print the result to terminal

# Valid commands
After running the program you are promtet with a "input: " here you write the numbers and operation you want to use.
the program does not take in the word "and" and it also only accept lower case letters. Note also that the program does not take inn the and in "one hundred and twenty three" but "one hundred ninty three"


valid operations
	+ = "plus"
	- = "minus"
	* = "times"
	/ = "over"


exampels:

	input: one hundred twenty three plus ninty nine
	Result: 222.00

	input: seven minus ten
	Result: -3.00

	input: four hundred times forty two
	Result: 16800.00

	input: seven over ten
	Result: 0.70

# All inputs allowed:
Input can be used in a combination up to nine hundred ninty nine

one, two, three, four, five, six, seven, eight, nine, ten
elleven, twelv, thirteen, forteen, fifteen, sixteen, seventeen, eighteen, nineteen
twenty, thirty, fourty, fifty, sixty, eighty, ninty, hundred
plus, minus, times, over

# Creator
Alexander Øvergård
23/08/2022
