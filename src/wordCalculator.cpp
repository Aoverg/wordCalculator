#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <algorithm>
#include <iomanip>

// char and vectors that is used to find what word fits what number and operation
char mathProblem[100];
std::vector<std::string> value1 = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
std::vector<std::string> value10 = {"twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninty"};
std::vector<std::string> value100 = {"hundred"};
std::vector<std::string> operation = {"plus", "minus", "times", "over"};

// split the string into several words to decypher
std::vector<std::string> splitString(char *mathProblem)
{
	std::vector<std::string> word;

	char *ptr;
	ptr = strtok(mathProblem, " "); // put the string up to first space (" ") in ptr

	while (ptr != NULL)
	{
		word.push_back(ptr); // move string to each corresponding vector
		ptr = strtok(NULL, " ");
	}
	return word;
}

double convert(std::vector<std::string> word)
{
	// flags and values used to count each word
	int num1 = 0;
	int num2 = 0;
	int symbol;
	double answer;
	int index = 0;
	bool gotOperation = true;

	for (int i = 0; i < word.size(); i++) // go thorugh each word
	{
		// serach for the word in each vector and add its value to either num1 or 2
		auto itr = std::find(value1.begin(), value1.end(), word[i]);
		if (itr != value1.end())
		{
			if (gotOperation)
				num1 += itr - value1.begin(); // itr value is showing the place in memory and by subtracting the beginning value we find what number it corresponds to
			else
				num2 += itr - value1.begin();
		}

		itr = std::find(value10.begin(), value10.end(), word[i]);
		if (itr != value10.end())
		{
			if (gotOperation)
				num1 += ((itr - value10.begin() + 2) * 10); // value10 starts with 20 so we need to add 2 and time by 10 to get the correct value
			else
				num2 += ((itr - value10.begin() + 2) * 10);
		}

		itr = std::find(value100.begin(), value100.end(), word[i]);
		if (itr != value100.end())
		{
			// here i went with an assumtion that hundred always follow one number and no more.
			//  so i made an easy and not pretty code that either adds 100 or times by 100
			// depending on of the first number is set before hand
			if (gotOperation)
			{
				if (num1 > 0)
					num1 *= 100;
				else
					num1 += 100;
			}
			else
			{
				if (num2 > 0)
					num2 *= 100;
				else
					num2 += 100;
			}
		}

		// not inportant what value operation gets as long as i use the correct value in the switch case later
		itr = std::find(operation.begin(), operation.end(), word[i]);
		if (itr != operation.end())
		{
			symbol = itr - operation.begin();
			gotOperation = false;
		}
	}

	// switch case to end the operation
	switch (symbol)
	{
	case 0:
		answer = num1 + num2;
		break;
	case 1:
		answer = num1 - num2;
		break;
	case 2:
		answer = num1 * num2;
		break;
	case 3:
		answer = (float) num1 / (float) num2;
		break;
	default:
		break;
	}

	return answer;
}

int main()
{
	// get value in word
	std::cout << "Input: ";
	std::cin.getline(mathProblem, sizeof(mathProblem));

	// split the string into several pieces in a vector
	std::vector<std::string> word = splitString(mathProblem);

	// converts the words into numbers and do the corresponding operation
	double answer = convert(word);

	// print result to terminal
	std::cout << std::fixed;
    std::cout << std::setprecision(2);
	std::cout << "Result: " << answer << std::endl;
}
